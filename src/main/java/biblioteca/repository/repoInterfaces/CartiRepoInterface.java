package biblioteca.repository.repoInterfaces;


import biblioteca.model.Carte;

import java.io.IOException;
import java.util.List;

public interface CartiRepoInterface {
	void adaugaCarte(Carte c) throws IOException;
	void modificaCarte(Carte nou, Carte vechi);
	void stergeCarte(Carte c);
	List<Carte> cautaCarte(String ref);
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(Integer an);

    void clear();
}
