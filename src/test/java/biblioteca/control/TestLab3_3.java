package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestLab3_3 {
    private static CartiRepoInterface cr ;

    @BeforeClass
    public static void initialize(){
        cr = new CartiRepoMock();

    }

    @Test
    public void testCauta(){
        List<Carte> books = cr.cautaCarte("Calinescu");
        assertEquals(2,books.size());
    }
}
