package biblioteca.control;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestBVA3 {
    private CartiRepoInterface cr = new CartiRepoMock();
    BibliotecaCtrl bibliotecaCtrl = new BibliotecaCtrl(cr);
    @Test
    public void testAdd() throws Exception {
        int size=bibliotecaCtrl.getCarti().size();
        Carte c= new Carte();
        c.setTitlu("Povesti");
        c.setEditura("Corint");
        c.setAnAparitie(1202);
        c.setReferenti(new ArrayList<String>(){{add("Ion Creanga"); add("Hector Malot");}});
        c.setCuvinteCheie(new ArrayList<String>(){{add("copil"); add("batran");}});

        try {
            bibliotecaCtrl.adaugaCarte(c);
            assertEquals(size+1, bibliotecaCtrl.getCarti().size());
        }
        catch (Exception ex){
            assertEquals(size, bibliotecaCtrl.getCarti().size());
        }
    }
}
