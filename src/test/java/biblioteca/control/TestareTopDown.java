package biblioteca.control;

import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestareTopDown {
    public static TestCaseECPValidCarteValida testCaseECPValidCarteValida;
    public static TestLab3_2 testLab3_2;
    public static TestLab4_Valid testLab4_valid;
    public static CartiRepoInterface cr = new CartiRepoMock();
    public static BibliotecaCtrl bibliotecaCtrl= new BibliotecaCtrl(cr);
    @BeforeClass
    public static void initialize(){
        testCaseECPValidCarteValida = new TestCaseECPValidCarteValida(cr,bibliotecaCtrl);
        testLab3_2 = new TestLab3_2();
        testLab3_2.setCont(cr);
        testLab4_valid = new TestLab4_Valid(cr);
    }

    @Test
    public void testUnitModuleA(){
        try {
            testCaseECPValidCarteValida.testAdd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testareIntegrareAB(){
        try {
            testCaseECPValidCarteValida.testAdd();
            testLab3_2.testCauta();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Test
    public void testareIntegrareABC(){
        try{
            testCaseECPValidCarteValida.testAdd();
            testLab3_2.testCauta();
            testLab4_valid.testAfisare();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
