package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestCaseECPValidCarteValida {
    public static CartiRepoInterface cr;
    public static BibliotecaCtrl bibliotecaCtrl;
    @BeforeClass
    public static void initialize(){
        cr = new CartiRepoMock();
        bibliotecaCtrl = new BibliotecaCtrl(cr);
    }

    public TestCaseECPValidCarteValida(){

    }

    public TestCaseECPValidCarteValida(CartiRepoInterface cr, BibliotecaCtrl br){
        this.cr = cr;
        this.bibliotecaCtrl=br;
    }

    @Test
    public void testAdd() throws Exception {
        int size=bibliotecaCtrl.getCarti().size();
        Carte c= new Carte();
        c.setTitlu("Povesti");
        c.setEditura("Corint");
        c.setAnAparitie(1945);
        c.setReferenti(new ArrayList<String>(){{add("Ion Creanga"); add("Hector Malot");}});
        c.setCuvinteCheie(new ArrayList<String>(){{add("copil"); add("batran");}});
        bibliotecaCtrl.adaugaCarte(c);
        assertEquals(size+1,bibliotecaCtrl.getCarti().size());
    }
}
