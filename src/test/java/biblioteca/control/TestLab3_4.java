package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestLab3_4 {
    private static CartiRepoInterface cr ;

    @BeforeClass
    public static void initialize(){
        cr = new CartiRepoMock();
        List<String> cuv = new ArrayList<String>();
        cuv.add("enigma");
        cuv.add("otilia");
        cuv.add("avere");
        List<String> ref = new ArrayList<String>();
        try {
            cr.adaugaCarte(new Carte("Enigma Otiliei", ref,1948,"Litera",cuv));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCauta(){
        List<Carte> books = cr.cautaCarte("Calinescu");
        assertEquals(2,books.size());
    }
}
