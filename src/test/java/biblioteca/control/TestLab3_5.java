package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestLab3_5 {
    private static CartiRepoInterface cr ;

    @BeforeClass
    public static void initialize(){
        cr = new CartiRepoMock();
       cr.clear();
    }

    @Test
    public void testCauta(){
        List<Carte> books = cr.cautaCarte("Calinescu");
        assertEquals(0,books.size());
    }
}
