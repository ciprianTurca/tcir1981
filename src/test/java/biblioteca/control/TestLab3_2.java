package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestLab3_2 {
    public static CartiRepoInterface cr ;

    @BeforeClass
    public static void initialize(){
        cr = new CartiRepoMock();

    }

    public void setCont(CartiRepoInterface repoInterface){
        cr=repoInterface;
    }

    @Test
    public void testCauta(){
        List<Carte> books = cr.cautaCarte("Eminescu");
        assertEquals(1,books.size());
    }
}
