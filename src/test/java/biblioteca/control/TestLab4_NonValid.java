package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestLab4_NonValid {
    private static CartiRepoInterface cr ;

    @BeforeClass
    public static void initialize(){
        cr = new CartiRepoMock();
    }

    @Test
    public void testAfisare(){
        List<Carte> books = cr.getCartiOrdonateDinAnul(1234);
        assertEquals(0,books.size());
    }
}
